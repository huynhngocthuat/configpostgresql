package com.assetjapan.configpostgresql.services;

import com.assetjapan.configpostgresql.dtos.ProductDto;
import com.assetjapan.configpostgresql.models.Product;
import com.assetjapan.configpostgresql.repositories.ProductRepository;
import com.assetjapan.configpostgresql.utils.httpResponse.exceptions.NotFoundException;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Long createNewProduct(ProductDto productDto) {
//        System.out.println(productDto.getId()+ productDto.getName()+ productDto.getName());
//        Optional<Product> foundProduct = productRepository.findProductById(productDto.getId());
//        if(!foundProduct.isPresent()){
//            throw new NotFoundException("Product with id " + productDto.getId() + "does not existed !!!");
//        }

        Product product = new Product();
        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());

        return productRepository.save(product).getId();
    }

    public void deleteProduct(Long idProduct){
        Optional<Product> foundProduct = productRepository.findProductById(idProduct);
        if(!foundProduct.isPresent()){
            System.out.println("Does not existed");
        }
        productRepository.deleteById(idProduct);
    }

    public void updateProduct(ProductDto productDto){
        Optional<Product> foundProduct = productRepository.findProductById(productDto.getId());
        if(!foundProduct.isPresent()){
            System.out.println("Does not existed");
        }
        foundProduct.get().setName(productDto.getName());
        foundProduct.get().setDescription(productDto.getDescription());
        productRepository.save(foundProduct.get());
    }
}
