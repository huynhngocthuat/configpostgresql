package com.assetjapan.configpostgresql.controllers;

import com.assetjapan.configpostgresql.dtos.ProductDto;
import com.assetjapan.configpostgresql.models.Product;
import com.assetjapan.configpostgresql.repositories.ProductRepository;
import com.assetjapan.configpostgresql.services.ProductService;
import com.assetjapan.configpostgresql.utils.httpResponse.NoContentResponse;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    public List<Product> getAllProducts() {
        return productService.findAll();
    }

    @PostMapping("/create")
    public ProductDto createNewProduct(@RequestParam String name,
                                       @RequestParam String description){
        ProductDto productDto = new ProductDto(1L, name, description);
        Long id = this.productService.createNewProduct(productDto);
        productDto.setId(id);
        return productDto;
    }

    @DeleteMapping("delete")
    public String deleteProduct(@RequestParam Long id){
        this.productService.deleteProduct(id);
        return "DELETE SUCCESS";
    }

    @PutMapping("update")
    public ProductDto updateProduct(@RequestParam Long id,
                                @RequestParam String name,
                                @RequestParam String description){
        ProductDto productDto = new ProductDto(id, name, description);
        this.productService.updateProduct(productDto);

        return productDto;
    }
}
