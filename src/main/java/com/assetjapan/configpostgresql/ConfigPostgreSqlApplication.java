package com.assetjapan.configpostgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigPostgreSqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigPostgreSqlApplication.class, args);
    }

}
